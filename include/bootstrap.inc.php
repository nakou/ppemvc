<?php
/**
 * Fonctions les plus basiques, nécéssaire au bon fonctionnement du site
 *
 * @package     default
 * @author      CC
 * @version     0.1
 */

/**
 * Routeur permettant d'appeller le controleur rechercher
 *
 **/
    function routeur($controler = NULL, $esTconnecter = FALSE, $action = NULL,$params = NULL){
        if(DEBUGMOD){
        echo "GLOBAL DIR";
        var_dump(__DIR__."\..\controleurs\c_".$controler.".php");}
        
        if($esTconnecter && isset($controler)){
            include(__DIR__."\..\controleurs\c_".$controler.".php");
            
            if(isset($action)){
                if(is_callable($action))
                    call_user_func_array($action, $params);
            } else {
                call_user_func('index');
            }
            
        }else{
            include(__DIR__."\..\controleurs\c_connexion.php");
            call_user_func('index');
            
        }
    }
?>