<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utilisateur
 *
 * @author Dryk
 */
class Utilisateur {
        
        protected $id;
	protected $firstName;
	protected $lastName;
	protected $login;
	protected $adress;
	protected $zipCode;
	protected $city;
	protected $hireDate;
        protected $specialisation;
        protected $correctlyLoad = false;
        protected $mdpsha;
        

                
        public function __construct($utilId = 0, $isNew = false) {
            if($isNew){
                
            } else {
                $pdo = PdoGsb::getPDOobject();
                $sql = $pdo->prepare('SELECT * 
                                        FROM utilisateur 
                                        WHERE utilId = :utilId');
                $sql->execute(array(':utilId' => $utilId));


                $resultArray = $sql->fetch(PDO::FETCH_ASSOC);

                $this->id = $resultArray['utilId'];
                $this->lastName = $resultArray['utilNom'];
                $this->firstName = $resultArray['utilPrenom'];
                $this->login = $resultArray['utilLogin'];
                $this->adress = $resultArray['utilAdresse'];
                $this->zipCode = $resultArray['utilCp'];
                $this->city = $resultArray['utilVille'];
                $this->hireDate = $resultArray['utilDateEmbauche'];
                $this->specialisation = $resultArray['utilSpecialisation'];

                $this->correctlyLoad = TRUE;
            }
        }

        
        public static function checkLoginPassword($login, $password){
            $pdo = PdoGsb::getPDOobject();
            if(DEBUGMOD){ echo "VAR DUMP PDO FUNCTION CHECK LOGIN";
                var_dump($pdo);}
            
           // $resultArray = array();
            
            $login = addslashes(trim($login));
            if(DEBUGMOD){ echo "VAR DUMP LOGIN FUNCTION CHECKLOGINPASSWORD</br>";
                var_dump($login);}
            
            $password = sha1(addslashes(trim($password)));
            if(DEBUGMOD){ echo "VAR DUMP PASSWORD FUNCTION CHECKLOGINPASSWORD</br>";
                var_dump($password);}
            
            
            $sql = $pdo->prepare('SELECT utilId 
                                    FROM utilisateur
                                    WHERE utilLogin = :login  
                                    AND utilMdp = :password');
            $sql->execute(array(':login' => $login,
                                ':password' => $password));
            

            $resultArray = $sql->fetch(PDO::FETCH_ASSOC);
            
            if (!empty($resultArray)) {
			$resultArray = $resultArray['utilId'];
                        $password = sha1(addslashes(trim($password)));
                        if(DEBUGMOD){ echo "UTIL.CLASS.CHECKLP.TRUE</br>";}
		} else {
			$resultArray = FALSE;
                        if(DEBUGMOD){ echo "UTIL.CLASS.CHECKLP.FALSE</br>";}
		}
		
		return $resultArray;
        }
        
	/**
	* Sync data from class to database
	*/
	public function syncDatabase(){
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare('UPDATE `utilisateur` 
                                   SET utilNom = :utilNom, utilPrenom = :utilPrenom,
                                   utilAdresse = :utilAdresse, utilCp = :utilCp,
                                   utilVille = :utilVille, utilDateEmbauche = :utilDateEmbauche,
                                   utilSpecialisation = :utilSpe
                                   WHERE utilId = :utilId' );
            
            $sql->execute(array(':utilNom' => $this->lastName,
                                ':utilPrenom' => $this->firstName,
                                ':utilAdresse' => $this->adress,
                                ':utilCp'=>$this->zipCode,
                                ':utilVille'=>$this->city,
                                ':utilDateEmbauche'=>$this->hireDate,
                                ':utilSpe'=>$this->specialisation,
                                ':utilId'=>$this->id));
	}
        
	/**
	* Sync data from database to class
	*/
	public function syncClass(){
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare('SELECT * 
                                    FROM utilisateur 
                                    WHERE utilId = :utilId');
            $sql->execute(array(':utilId' => $this->id));

            
            $resultArray = $sql->fetch(PDO::FETCH_ASSOC);
            
            $this->id = $resultArray['utilId'];
            $this->lastName = $resultArray['utilNom'];
            $this->firstName = $resultArray['utilPrenom'];
            $this->login = $resultArray['utilLogin'];
            $this->adress = $resultArray['utilAdresse'];
            $this->zipCode = $resultArray['utilCp'];
            $this->city = $resultArray['utilVille'];
            $this->hireDate = $resultArray['utilDateEmbauche'];
            $this->specialisation = $resultArray['utilSpecialisation'];
            
            $this->correctlyLoadd = TRUE;
	}        
        
        public function saveNewObject(){
        $pdo = PdoGsb::getPDOobject();
        $sql = $pdo->prepare('INSERT INTO utilisateur
                                (utilNom, utilPrenom, utilLogin, utilMdp utilAdresse,utilCp,idEtat,utilVille,utilDateEmbauche,utilSpecialisation)
                             VALUES (:utilNom, :utilPrenom, :utilLogin, :utilMdp, :utilAdresse, :utilCp, :utilVille,  :utilDateEmbauche, :utilSpe)');
            $sql->execute(array(':utilNom' => $this->lastName,
                                ':utilPrenom' => $this->firstName,
                                ':utilLogin' => $this->login,
                                ':utilMdp' => $this->mdpsha,
                                ':utilAdresse' => $this->adress,
                                ':utilCp'=>$this->zipCode,
                                ':utilVille'=>$this->city,
                                ':utilDateEmbauche'=>$this->hireDate,
                                ':utilSpe'=>$this->specialisation,
                                ':utilId'=>$this->id));
        }
        
        public function definePassword($mdp){
            $this->mdpsha = sha1($mdp);
        }
        
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function getFirstName() {
            return $this->firstName;
        }

        public function setFirstName($firstName) {
            $this->firstName = $firstName;
        }

        public function getLastName() {
            return $this->lastName;
        }

        public function setLastName($lastName) {
            $this->lastName = $lastName;
        }

        public function getLogin() {
            return $this->login;
        }

        public function setLogin($login) {
            $this->login = $login;
        }

        public function getAdress() {
            return $this->adress;
        }

        public function setAdress($adress) {
            $this->adress = $adress;
        }

        public function getZipCode() {
            return $this->zipCode;
        }

        public function setZipCode($zipCode) {
            $this->zipCode = $zipCode;
        }

        public function getCity() {
            return $this->city;
        }

        public function setCity($city) {
            $this->city = $city;
        }

        public function getHireDate() {
            return $this->hireDate;
        }

        public function setHireDate($hireDate) {
            $this->hireDate = $hireDate;
        }

        public function getSpecialisation() {
            return $this->specialisation;
        }

        public function setSpecialisation($specialisation) {
            $this->specialisation = $specialisation;
        }
}

