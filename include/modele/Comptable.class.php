<?php

/**
*
* Class Comptable 
* @version 1.0
* @author  Nakou
*/

class Comptable extends Utilisateur{
	
        public static function isComptable($utilId){
            $sql = PdoGsb::getPDOobject()->prepare('SELECT * 
                                    FROM utilisateur 
                                    WHERE visitId = :utilId');
            $sql->execute(array(':utilId' => $utilId));
            $resultArray = $sql->fetch(PDO::FETCH_ASSOC);
            if(empty($resultArray))
                return true;
            else
                return false;
        }
        
	/**
	 * Check all the users who the First name or the last name look like the searchname
	 * @param $searchName 
	 * @return Comptables Array.
	 */
	public static function findComptableCloseTo($searchName){
            $tabofVisitors = array();
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare('SELECT utilId 
                                    FROM utilisateur 
                                    WHERE utilNome LIKE :search 
                                    OR utilPrenom LIKE :search');
            $sql->execute(array(':search' => $searchName));
            $resultArray = $request->fetchAll();
            foreach($resultArray as $key => $value)
            {
                if(Comptable::isComptable($id)){
                    $tabofVisitors[] = new Comptable($utilId);
                }                
            }
	}
}