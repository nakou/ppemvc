<?php

/**
 * Description of Visiteur
 *
 * @author Nakou
 */
class Visiteur extends Utilisateur{

        private $rang;
        private $rangLibelle;
        private $ficheDeFraisActive;
        private $listeFicheDeFrais;
        
        public function __construct($visId = 0, $isNewVis = false) {
            if($isNewVis){
                
            } else {
                parent::__construct($visId, $isNewVis);

                $pdo = PdoGsb::getPDOobject();
                $sql = $pdo->prepare('SELECT rang_rangId,  rangLibelle
                                        FROM visiteur, rang
                                        WHERE visitId = :visitId AND rang.rangId = visiteur.rang_rangId');
                $sql->execute(array(':visitId' => $visId));


                $resultArray = $sql->fetch(PDO::FETCH_ASSOC);
                if(DEBUGMOD){ echo "VISIT.CLASS.CONSTRUCT.RESULT.REQUET.RANG";
                                var_dump($resultArray);}
                                
                $this->rang = $resultArray['rang_rangId'];
                $this->rangLibelle = $resultArray['rangLibelle'];
                

                //Chargement fiche de frais
                
                $date = getdate();
                $month = $date['month'];
                
                if(FicheFrais::exist($this->id, $month)){
                    if(DEBUGMOD){ echo "VISIT.CLASS.FICHE.FRAIS.EXIST";}
                    $this->ficheDeFraisActive = new FicheFrais($this->id, $month);
                }else{
                    if(DEBUGMOD){ echo "VISIT.CLASS.FICHE.FRAIS.DOESNT.EXIST";}
                    $this->ficheDeFraisActive = new FicheFrais($this->id, $month, TRUE);
                }
		
		$this->listeFicheDeFrais = FicheFrais::returnAllFichesForID($this->id);
                
                $this->correctlyLoad = TRUE;
                //$this->callSpecific();
            }
        }       
        
        public static function isVisiteur($utilId){
            $sql = PdoGsb::getPDOobject()->prepare('SELECT * 
                                    FROM utilisateur 
                                    WHERE visitId = :utilId');
            $sql->execute(array(':utilId' => $utilId));
            $resultArray = $sql->fetch(PDO::FETCH_ASSOC);
            if(empty($resultArray))
                return true;
            else
                return false;
        }
        
	/**
	 * Check all the users who the First name or the last name look like the searchname
	 * @param $searchName 
	 * @return Comptables Array.
	 */
	public static function findVisiteurCloseTo($searchName){
            $tabofVisitors = array();
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare('SELECT utilId 
                                    FROM utilisateur 
                                    WHERE utilNome LIKE :search 
                                    OR utilPrenom LIKE :search');
            $sql->execute(array(':search' => $searchName));
            $resultArray = $request->fetchAll();
            foreach($resultArray as $key => $value)
            {
                if(Visiteur::isVisiteur($id)){
                    $tabofVisitors[] = new Visiteur($utilId);
                }                
            }
	}
        
        public function getRang() {
            return $this->rang;
        }

        public function setRang($rang) {
            $this->rang = $rang;
        }

        public function getRangLibelle() {
            return $this->rangLibelle;
        }

        public function setRangLibelle($rangLibelle) {
            $this->rangLibelle = $rangLibelle;
        }

        public function getFicheDeFraisActive() {
            return $this->ficheDeFraisActive;
        }

        public function setFicheDeFraisActive($ficheDeFraisActive) {
            $this->ficheDeFraisActive = $ficheDeFraisActive;
        }

        public function getListeFicheDeFrais() {
            return $this->listeFicheDeFrais;
        }

        public function setListeFicheDeFrais($listeFicheDeFrais) {
            $this->listeFicheDeFrais = $listeFicheDeFrais;
        }


}
