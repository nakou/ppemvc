<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LigneFraisForfait
 *
 * @author drikt_000
 */
class LigneFraisForfait {
    private $idFraisForfait;
    private $idVisiteur;
    private $mois;
    private $quantite;
    private $libelle;
    private $montant;
    
    public function __construct($idVisiteur, $mois, $idFF, $quantite = 1, $isNew = false) {
        if($isNew){
            
            if(DEBUGMOD){
                echo "CLASS.LIGNE.NEW.</br>";}
            
            $this->idVisiteur = $idVisiteur;
            $this->mois = $mois;
            $this->idFraisForfait = $idFF;
            $this->quantite = $quantite;
            
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare("SELECT libelle, montant
                                    FROM fraisforfait
                                    WHERE id = :idFraisForfait");
            
            $sql->bindParam(':idFraisForfait', $this->idFraisForfait);
            $sql->execute();
            
            while($resultArray = $sql->fetch(PDO::FETCH_ASSOC)){
                 $this->libelle = $resultArray['libelle'];
                 $this->montant = $resultArray['montant'];
            }           
            
            //Ajout en base
            $sql = NULL;
            $sql = $pdo->prepare("INSERT INTO lignefraisforfait
                                   VALUES(:idVisiteur, 
                                            :mois,
                                            :idFraisForfait,
                                            :quantite)");
            
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            $sql->bindParam(':idFraisForfait', $this->idFraisForfait);
            $sql->bindParam(':quantite', $this->quantite);

            $sql->execute();
            
        } else {
            
             if(DEBUGMOD){
                echo "CLASS.LIGNE.FORF.CHARGEMENT.</br>";}
            
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare("SELECT idFraisForfait, quantite, libelle, montant 
                                    FROM lignefraisforfait, fraisforfait
                                    WHERE lignefraisforfait.idVisiteur = :idVisiteur
                                    AND lignefraisforfait.mois = :mois
                                    AND fraisforfait.id = :idFF");
            
            $sql->bindParam(':idVisiteur', $idVisiteur);
            $sql->bindParam(':mois', $mois);
            $sql->bindParam(':idFF', $idFF);
            
            $sql->execute();

                 $resultArray = $sql->fetch(PDO::FETCH_ASSOC);
                 if(DEBUGMOD){
                    echo "CLASS.LIGNE.FORF.RESULT.REQUETE</br>";
                    var_dump($resultArray);}
                 $this->idFraisForfait = $idFF;
                 $this->idVisiteur = $idVisiteur;
                 $this->$mois = $mois;
                 $this->quantite = $resultArray['quantite'];
                 $this->libelle = $resultArray['libelle'];
                 $this->montant = $resultArray['montant'];
                       
        }
    }
    
    public function syncDatabase(){
        $pdo = PdoGsb::getPDOobject();
        $sql = $pdo->prepare("UPDATE lignefraisforfait
                                SET idVisiteur = :idVisiteur, 
                                    mois = :mois,
                                    idFraisForfait = :idFraisForfait,
                                    quantite = :quantite
                                WHERE idVisiteur = :idVisiteur
                                    AND mois = :mois
                                    AND idFraisForfait = :idFraisForfait");
                              
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            $sql->bindParam(':idFraisForfait', $this->idFraisForfait);
            $sql->bindParam(':quantite', $this->quantite);

            $sql->execute();    

    }
    
    public static function getTousLesTypes(){
        $lesTypes = array();
        
        $pdo = PdoGsb::getPDOobject();
        $sql = $pdo->prepare('SELECT libelle
                                FROM fraisforfait');

        $sql->execute();
        
        while($resultArray = $sql->fetch(PDO::FETCH_ASSOC)){
            $lesTypes[] = $resultArray['libelle'];
        }
        return $lesTypes;
    }
    public function getIdFraisForfait() {
        return $this->idFraisForfait;
    }

    public function setIdFraisForfait($idFraisForfait) {
        $this->idFraisForfait = $idFraisForfait;
    }

    public function getIdVisiteur() {
        return $this->idVisiteur;
    }

    public function setIdVisiteur($idVisiteur) {
        $this->idVisiteur = $idVisiteur;
    }

    public function getIdMonth() {
        return $this->idMonth;
    }

    public function setIdMonth($idMonth) {
        $this->idMonth = $idMonth;
    }

    public function getQuantite() {
        return $this->quantite;
    }

    public function setQuantite($quantite) {
        $this->quantite = $quantite;
    }
    
    public function ajoutQuantite($plus){
        $this->quantite += $plus;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    public function getMontant() {
        return $this->montant;
    }
    
    public function getMontantTotal() {
        return $this->montant * $this->quantite;
    }

    public function setMontant($montant) {
        $this->montant = $montant;
    }

}

?>
