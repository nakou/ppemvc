<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LigneFraisForfait
 *
 * @author drikt_000
 */
class LigneFraisHorsForfait {
    private $id;
    private $idVisiteur;
    private $mois;
    private $date;
    private $libelle;
    private $montant;
    
    public function __construct($idVisiteur, $mois = null, $date = null, $isNew = false, $id = null, $libelle = null, $montant = null) {
        if($isNew){
            
            if(DEBUGMOD){
                echo "CLASS.LIGNEHF.NEW.</br>";}
            
            $this->idVisiteur = $idVisiteur;
            $this->mois = $mois;
            $this->libelle = $libelle;
            $this->montant = $montant;
            $this->date = $date;
            
            $pdo = PdoGsb::getPDOobject();          
            
            //Ajout en base
            $sql = $pdo->prepare("INSERT INTO lignefraishorsforfait(idVisiteur, mois, libelle, date, montant)
                                   VALUES(:idVisiteur, 
                                            :mois,
                                            :libelle,
                                            :date,
                                            :montant)");
            
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            $sql->bindParam(':libelle', $this->libelle);
            $sql->bindParam(':date', $this->date);
            $sql->bindParam(':montant', $this->montant);

            $sql->execute();
            
            //recuperation de l'id
            $sql = null;
            $sql = $pdo->prepare("SELECT id 
                                    FROM lignehorsfraisforfait
                                    WHERE idVisiteur = :idVisiteur
                                    AND mois = :mois
                                    AND date = :date
                                    AND montant = :montant
                                    AND libelle = :libelle");
            
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            $sql->bindParam(':libelle', $this->libelle);
            $sql->bindParam(':date', $this->date);
            $sql->bindParam(':montant', $this->montant);
            
            $sql->execute();
            
            $this->id = $resultArray['id'];
        } else {
            
             if(DEBUGMOD){
                echo "CLASS.LIGNE.HF.CHARGEMENT.</br>";}
            
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare("SELECT *
                                    FROM lignehorsfraisforfait
                                    WHERE id = :id");
            
            $sql->bindParam(':id', $id);
            
            $sql->execute();

                 $resultArray = $sql->fetch(PDO::FETCH_ASSOC);
                 if(DEBUGMOD){
                    echo "CLASS.LIGNE.FORF.RESULT.REQUETE</br>";
                    var_dump($resultArray);}
                 $this->id = $id;
                 $this->idVisiteur = $resultArray['idVisiteur'];
                 $this->mois = $resultArray['mois'];
                 $this->date = $resultArray['date'];
                 $this->libelle = $resultArray['libelle'];
                 $this->montant = $resultArray['montant'];
                       
        }
    }
    
    public function syncDatabase(){
        $pdo = PdoGsb::getPDOobject();
        $sql = $pdo->prepare("UPDATE lignefraishorsforfait
                                SET idVisiteur = :idVisiteur, 
                                    mois = :mois,
                                    date = :date,
                                    libelle = :libelle,
                                    montant = :montant
                                WHERE id = :id");
                              
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            $sql->bindParam(':date', $this->date);
            $sql->bindParam(':libelle', $this->libelle);
            $sql->bindParam(':montant', $this->montant);

            $sql->execute();    

    }
    
    public static function getTousLesTypes(){
        $lesTypes = array();
        
        $pdo = PdoGsb::getPDOobject();
        $sql = $pdo->prepare('SELECT libelle
                                FROM fraisforfait');

        $sql->execute();
        
        while($resultArray = $sql->fetch(PDO::FETCH_ASSOC)){
            $lesTypes[] = $resultArray['libelle'];
        }
        return $lesTypes;
    }
    public function getIdFraisForfait() {
        return $this->idFraisForfait;
    }

    public function setIdFraisForfait($idFraisForfait) {
        $this->idFraisForfait = $idFraisForfait;
    }

    public function getIdVisiteur() {
        return $this->idVisiteur;
    }

    public function setIdVisiteur($idVisiteur) {
        $this->idVisiteur = $idVisiteur;
    }

    public function getIdMonth() {
        return $this->idMonth;
    }

    public function setIdMonth($idMonth) {
        $this->idMonth = $idMonth;
    }

    public function getQuantite() {
        return $this->quantite;
    }

    public function setQuantite($quantite) {
        $this->quantite = $quantite;
    }
    
    public function ajoutQuantite($plus){
        $this->quantite += $plus;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    public function getMontant() {
        return $this->montant;
    }
    
    public function getMontantTotal() {
        return $this->montant * $this->quantite;
    }

    public function setMontant($montant) {
        $this->montant = $montant;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getMois() {
        return $this->mois;
    }

    public function setMois($mois) {
        $this->mois = $mois;
    }

    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
    }


}

?>
