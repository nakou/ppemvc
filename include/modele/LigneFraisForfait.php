<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LigneFraisForfait
 *
 * @author drikt_000
 */
class LigneFraisForfait {
    private $idFraisForfait;
    private $idVisiteur;
    private $idMonth;
    private $quantite;
    private $libelle;
    private $montant;
    
    public function __construct($idVisiteur, $mois, $idFF, $isNew = false) {
        if($isNew){
            
            $this->pdo = PdoGsb::getPDOobject();
            $this->idVisiteur = $idVisiteur;
            $this->mois = $mois;
            $this->idFraisForfait = $idFF;
            $this->quantite = 1;
            
            //Ajout en base
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare('INSERT INTO (`lignefraisforfait` 
                                   VALUES :idVisiteur, 
                                            :mois,
                                            :idFraisForfait,
                                            :quantite)');
            
            $sql->execute(array(':idVisiteur' => $this->idVisiteur,
                                ':mois' => $this->mois,
                                ':idFraisForfait' => $this->idFraisForfait,
                                ':quantite'=>$this->quantite,
                                ':dateModif'=> date("dd/mm/yy")));
            
        } else {
            
            $this->pdo = PdoGsb::getPDOobject();
            $sql = $this->pdo->prepare('SELECT idFraisForfait, quantite, libelle, montant 
                                    FROM lignefraisforfait, fraisforfait
                                    WHERE idVisiteur = :idVisiteur
                                    AND mois = :mois
                                    AND idFraitForfait = :idFF');

            $sql->execute(array(':idVisiteur' => $idVisiteur,
                                ':mois' => $mois,
                                ':idFF' => $idFF));

            while($resultArray = $sql->fetch(PDO::FETCH_ASSOC)){
                 $this->idFraisForfait = $idFF;
                 $this->idVisiteur = $idVisiteur;
                 $this->idMonth = $mois;
                 $this->quantite = $resultArray['quantite'];
                 $this->libelle = $resultArray['libelle'];
                 $this->montant = $resultArray['montant'];
            }           
        }
    }
    
    public function syncDatabase(){
        $pdo = PdoGsb::getPDOobject();
        $sql = $pdo->prepare('UPDATE `lignefraisforfait` 
                                SET idVisiteur = :idVisiteur, 
                                    mois = :mois,
                                    idFraisForfait = :idFraisForfait,
                                    quantite = :quantite
                                WHERE idVisiteur = :idVisiteur
                                    AND mois = :mois
                                    AND idFraisForfait = :idFraisForfait');
            
            $sql->execute(array(':idVisiteur' => $this->idVisiteur,
                                ':mois' => $this->mois,
                                ':idFraisForfait' => $this->idFraisForfait,
                                ':quantite'=>$this->quantite,
                                ':dateModif'=> date("dd/mm/yy")));
    }
}

?>
