<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FicheFrais
 *
 * @author drikt_000
 */
class FicheFrais {
    
    private $idVisiteur;
    private $mois;
    private $ligneForfait;
    private $ligneHorsForfait;
    private $nbJustificatifs;
    private $montantValide;
    private $dateModif;
    private $idEtat;
    private $etat;
    
    
    
    public function __construct($idVisiteur, $mois, $isNew = false) {
        if($isNew){
            
            $this->idVisiteur = $idVisiteur;
            $this->mois = $mois;
            $this->ligneForfait = array();
            $this->ligneHorsForfait = array();
            $this->nbJustificatifs = 0;
            $this->montantValide = 0;
            $this->dateModif = date("Y-m-d");
            $this->idEtat = 3;
            $this->etat = 'Fiche créée, saisie en cours';
            
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare("INSERT INTO fichefrais 
                                   VALUES(:idVisiteur, 
                                            :mois,
                                            :nbJustificatifs,
                                            :montantValide, 
                                            :dateModif, 
                                            :idEtat)");
            
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            $sql->bindParam(':nbJustificatifs', $this->nbJustificatifs);
            $sql->bindParam(':montantValide', $this->montantValide);
            $sql->bindParam(':dateModif', $this->dateModif);
            $sql->bindParam(':idEtat', $this->idEtat);
            
            $sql->execute();
            
            
        } else {
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare('SELECT * 
                                    FROM fichefrais, etat 
                                    WHERE idVisiteur = :idVisiteur
                                    AND mois = :mois
                                    AND id = fichefrais.idEtat');

            $sql->execute(array(':idVisiteur' => $idVisiteur,
                                ':mois' => $mois));


            $resultArray = $sql->fetch(PDO::FETCH_ASSOC);

            $this->idVisiteur = $resultArray['idVisiteur'];
            $this->mois = $resultArray['mois'];
            $this->nbJustificatifs = $resultArray['nbJustificatifs'];
            $this->montantValide = $resultArray['montantValide'];
            $this->dateModif = $resultArray['dateModif'];
            $this->idEtat = $resultArray['idEtat'];
            $this->etat = $resultArray['libelle'];
            
            //ajout des lignes de frais forfait
            $sql = $pdo->prepare("SELECT idFraisForfait 
                                    FROM lignefraisforfait
                                    WHERE idVisiteur = :idVisiteur
                                    AND mois = :mois");
            
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            
            $sql->execute();
            
            while($resultArray = $sql->fetch(PDO::FETCH_ASSOC)){
                $this->ligneForfait[] = new LigneFraisForfait($this->idVisiteur,
                                                                $this->mois, 
                                                                $resultArray['idFraisForfait']);
            }
            
            
            //ajout lignes frais hrs forfait
            $sql = $pdo->prepare("SELECT id 
                                    FROM lignefraishorsforfait
                                    WHERE idVisiteur = :idVisiteur
                                    AND mois = :mois");
            
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            
            $sql->execute();
            
            while($resultArray = $sql->fetch(PDO::FETCH_ASSOC)){
                $this->ligneHorsForfait[] = new LigneHorsFraisForfait(null, 
                                                                        null, 
                                                                        null, 
                                                                        null, 
                                                                        $resultArray['id']);
            }
        }
    }
    
    static function exist($vis, $month){
        $sql = PdoGsb::getPDOobject()->prepare('SELECT * 
                                        FROM lignefraisforfait
                                        WHERE idVisiteur = :idVisiteur
                                            AND mois = :mois');
        
        $sql->execute(array(':idVisiteur' => $vis,
                                ':mois' => $month));
        
        if($resultArray = $sql->fetch(PDO::FETCH_ASSOC)){
            return true;
        }else{
            return false;
        }    
    }
    
    static function returnAllFichesForID($id){
	$sql = PdoGsb::getPDOobject()->prepare('SELECT * 
                                        FROM fichefrais
                                        WHERE idVisiteur = :idVisiteur');
        
        $sql->execute(array(':idVisiteur' => $id));
	$resultArray = $sql->fetch(PDO::FETCH_ASSOC);
	$liste = array();
	/* MAGIC */
	if(empty($resultArray['idVisiteur'])){ // Plusieurs fiches
	    foreach($resultArray as $ficheSQL){
		if(!empty($ficheSQL['idVisiteur']))
		{
		    $liste[] = new FicheFrais($ficheSQL['idVisiteur'],$ficheSQL['mois']);
		}
	    }
	} else { // une seule fiche
	    $liste[] = new FicheFrais($resultArray['idVisiteur'],$resultArray['mois']);
	}
	return $liste;
    }
    
    public function ajoutLigneForfait($idFraisForfait, $quantite){

        if(DEBUGMOD){
                echo "FICHE.CLASS.DEBUT.FCT.AJT";}

        if(count($this->ligneForfait) > 0){
            $existe = false;
            foreach ($this->ligneForfait as $ligne) {
                if($idFraisForfait == $ligne->getIdFraisForfait()){
                    $existe = $ligne;
                }
            }
            if($exist != false){

                if(DEBUGMOD){
                echo "FICHE.CLASS.AJOUT.IDFF";
                var_dump($idFraisForfait);}

                if(DEBUGMOD){
                echo "FICHE.CLASS.AJOUT.LIGNE.GET.IDFF";
                var_dump($ligne->getIdFraisForfait());}

                $exist->ajoutQuantite($quantite);
            } 

        }else{
            $this->ligneForfait[] = new LigneFraisForfait($this->idVisiteur, $this->mois, $idFraisForfait, $quantite, TRUE);
            if(DEBUGMOD){
                echo "FICHE.CLASS.ON.AJOUTE.UNE.LIGNE";
                var_dump($this->ligneForfait);}
        }     

    }
     
    public function ajoutLigneHorsForfait($idVis, $month, $libelle, $date, $montant){

        if(DEBUGMOD){
                echo "FICHE.CLASS.DEBUT.FCT.AJT.LIGNE.HF </br>";}

                    $this->ligneHorsForfait[] = new LigneFraisHorsForfait($idVis, $month, $date, $isNew = true, $id = null, $libelle, $montant);
                    if(DEBUGMOD){
                        echo "FICHE.CLASS.ON.AJOUTE.UNE.LIGNE.HF </br>";}
    }
    
    public function syncDatabase(){
        foreach ($this->ligneForfait as $ligne) {
            $ligne->syncDatabase();
        }
        foreach ($this->ligneHorsForfait as $ligne) {
            $ligne->syncDatabase();
        }
            $this->dateModif = date("Y-m-d");
            
            $pdo = PdoGsb::getPDOobject();
            $sql = $pdo->prepare("UPDATE fichefrais 
                                   SET  mois = :mois,
                                        nbJustificatifs = :nbJustificatifs,
                                        montantValide = :montantValide, 
                                        dateModif = :dateModif, 
                                        idEtat = :idEtat
                                   WHERE idVisiteur = :idVisiteur
                                        AND mois = :mois");
            
            $sql->bindParam(':idVisiteur', $this->idVisiteur);
            $sql->bindParam(':mois', $this->mois);
            $sql->bindParam(':nbJustificatifs', $this->nbJustificatifs);
            $sql->bindParam(':montantValide', $this->montantValide);
            $sql->bindParam(':dateModif', $this->dateModif);
            $sql->bindParam(':idEtat', $this->idEtat);
            
            $sql->execute();           
	}
    
    public function getFraisForfait(){
        $lesLignes = array();
        foreach ($this->ligneForfait as $ligne) {
            $lesLignes[] = $ligne;
        }
        return $lesLignes;
    }
    
    public function getFraisHorsForfait(){
        if($this->ligneHorsForfait != NULL){
        $lesLignes = array();
        foreach ($this->ligneHorsForfait as $ligne) {
            $lesLignes[] = $ligne;
        }
        return $lesLignes;
        } else {
            return NULL;
        }
        
    }
    
    public function getNbJustificaif(){
        return count($this->ligneHorsForfait);
    }

    public function getMontantTotal(){
        $montantTotal = 0;
        
        if(count($this->ligneForfait) > 0){
            foreach ($this->ligneForfait as $ligne) {
                $montantTotal += $ligne->getMontant() * $ligne->getQuantite();
            }
        }
        if(count($this->ligneHorsForfait) > 0){
            foreach ($this->ligneHorsForfait as $ligne) {
                $montantTotal += $ligne->getMontant();
            }
        }
        
        return $montantTotal;
    }

    public function getIdVisiteur() {
        return $this->idVisiteur;
    }

    public function setIdVisiteur($idVisiteur) {
        $this->idVisiteur = $idVisiteur;
    }

    public function getMois() {
        return $this->mois;
    }

    public function setMois($mois) {
        $this->mois = $mois;
    }

    public function getNbJustificatifs() {
        return $this->nbJustificatifs;
    }

    public function setNbJustificatifs($nbJustificatifs) {
        $this->nbJustificatifs = $nbJustificatifs;
    }

    public function getMontantValide() {
        return $this->montantValide;
    }

    public function setMontantValide($montantValide) {
        $this->montantValide = $montantValide;
    }

    public function getDateModif() {
        return $this->dateModif;
    }

    public function setDateModif($dateModif) {
        $this->dateModif = $dateModif;
    }

    public function getIdEtat() {
        return $this->idEtat;
    }

    public function setIdEtat($idEtat) {
        $this->idEtat = $idEtat;
    }

    public function getEtat() {
        return $this->etat;
    }

    public function setEtat($etat) {
        $this->etat = $etat;
    }


}

?>
