<?php
if(estConnecte()){ ?>
    <p class="navbar-text pull-right">
        Logged in as <a href="#" class="navbar-link"><?php echo $_SESSION['utilisateur']->getFirstName()."  ".$_SESSION['utilisateur']->getLastName()  ?></a>
        | <a href="index.php?uc=connexion&action=deconnexion" title="Se déconnecter" class="navbar-link">Déconnexion</a>
    </p>

<?php } else { ?>
    <p class="navbar-text pull-right">
        <a href="index.php?uc=connexion&action=index" class="navbar-link">Sign in</a>
    </p>
<?php } ?>