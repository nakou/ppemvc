<div class="container">
    <div class="row">
        <div class="span10 offset2">
	    <?php 
	    if(!empty($fiche)){ ?>
            <h4>Fiche pour le mois de <?php echo $month.' '.$year;?></h4>
	    <h6>Montant : <?php echo $fiche->getMontantValide();?></h6>
	    <h6>Dernière modif : <?php echo $fiche->getDateModif();?></h6>
	    <h6>Etat : <?php echo $fiche->getIdEtat();?></h6>
	    <h6>Nombre justificatif :  <?php echo $fiche->getNbJustificatifs();?></h6>
	    <?php } else {
		if(!empty($month))
		    echo "<h1>Pas de fiche pour le mois de ".$month.".</h1>";		
		else
		    echo "<h1>Aucune mois n'a été selectionné.</h1>";
	    } ?>
	</div>
    </div>
</div>