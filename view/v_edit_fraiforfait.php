        <div class="span10 offset2">
            <h3>Frais compris dans le forfait</h3>
            <table class="table-bordered">
                <thead>
                    <tr>
                        <th>Libelle</th>
                        <th>Montant</th>
                        <th>Quantité</th>
                        <th>Montant Total</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $lesLignes = $ficheActuelle->getFraisForfait();
                    foreach ($lesLignes as $ligne) {
                        echo ('<tr><td>'.$ligne->getlibelle().'</td><td>'.$ligne->getMontant().'</td><td>'.$ligne->getQuantite().'</td><td>'.$ligne->getMontantTotal().'</td></tr>');
                }?>
                </tbody>
            </table>
            
            <?php if(DEBUGMOD){
                            echo "LES LIGNES";
                            var_dump($lesLignes);} ?>
            
            <form method="POST" action="index.php?uc=fiche&action=ajoutLigneForfait">
                <p>
                    <label for="choixtypeforfait">Type de Forfait</label>
                    <select name="choixtypeforfait">
                        <?php 
                        $lesChoix = LigneFraisForfait::getTousLesTypes();
 
                        $numChoix = 1;
                        foreach ($lesChoix as $chois){
                            echo('<option value="'.$numChoix.'">'.$chois.'</option>');
                            $numChoix++;
                        }
                        ?>    
                    </select>
                    <?php if(DEBUGMOD){
                            echo "LES CHOIX";
                            var_dump($lesChoix);} ?>
                </p>
                <p>
                    <label for="quantite">Quantité</label>
                    <input id="quantite"  type="text"  name="quantite" size="30" maxlength="20">
                </p>
                <p>
                    <input type="submit" value="Valider" name="valider">
                    <input type="reset" value="Annuler" name="annuler"> 
                </p>
            </form>
            
        </div>
