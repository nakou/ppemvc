<div class="container">
    <div class="row">
        <div class="span10 offset2">
            <h2>Edition de la fiche de frais en cours</h2>
            <table>
                <tr>
                    <td>Mois</td>
                    <td><?php echo($ficheActuelle->getMois()); ?></td>
                </tr>
                <tr>
                    <td>Nombres de justificatifs</td>
                    <td><?php echo($ficheActuelle->getNbJustificaif()); ?></td>
                </tr>
                <tr>
                    <td>Montant</td>
                    <td><?php echo($ficheActuelle->getMontantTotal()); ?></td>
                </tr>
                <tr>
                    <td>Dernière modification</td>
                    <td><?php echo($ficheActuelle->getDateModif()); ?></td>
                </tr>
                <tr>
                    <td>Etat:</td>
                    <td><?php echo($ficheActuelle->getEtat()); ?></td>
                </tr>
            </table>
        </div>
        
        <?php include(__DIR__.'\v_edit_fraiforfait.php'); ?>
        
        <?php include(__DIR__.'\v_edit_fraishorsforfait.php'); ?>
        
    </div>
</div>