<div class="container">
    <div class="row">
        <div class="span10 offset2">
            <h2>Edition de la fiche en cours</h2>
            <p>Vous permet d'éditer votre fiche de frais du mois en cours.</p>
            <p><a class="btn" href="index.php?uc=fiche&action=edit">Edition</a></p>
        </div>
        <div class="span10 offset2">
            <h2>Consultation des fiches de frais</h2>
            <p>Vous permet de consulter la fiche de frais correspondant au mois choisie choisie.</p>
            <form method="POST" action=<?php echo('index.php?uc=fiche&action=afficherFiche');?>>
                <p>
                    <label for="month">Mois :</label>
                    <select name="month_select">
                        <option value="January">Janvier</option>
			<option value="February">Fevrier</option>
			<option value="March">Mars</option>
			<option value="April">Avril</option>
			<option value="May">Mai</option>
			<option value="June">Juin</option>
			<option value="July">Juillet</option>
			<option value="August">Aout</option>
			<option value="September">Septembre</option>
			<option value="October">Octobre</option>
			<option value="November">Novembre</option>
			<option value="December">Decembre</option>
			<?php 
			    
                        ?>
                       <!-- <option value="choix1">Choix 1</option> -->
                    </select>
                </p>
                <p>
                    <input type="submit" value="Valider" name="valider">
                </p>
            </form>
        </div>
        <div class="span10 offset2"><?php 
            if(DEBUGMOD){
                echo "SESSION UTIL";
                var_dump($_SESSION['utilisateur']);}
                ?>
        </div>
    </div>
</div>