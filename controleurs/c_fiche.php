<?php

    function index(){
        
    }
    
    function edit(){
        $date = getDate();
        $month = $date['month'];
        $ficheActuelle = $_SESSION['utilisateur']->getFicheDeFraisActive();
         
        include("view/v_header_navbar.php");
        include("view/v_edit_fiche.php");
        include("view/v_footer.php"); 
    }
    
    function ajoutLigneForfait(){
 
        $ficheActuelle = $_SESSION['utilisateur']->getFicheDeFraisActive();
        if(DEBUGMOD){
            echo "C.FICHE.FICHE.ACT</br>";
            var_dump($ficheActuelle);}
        if(DEBUGMOD){
            echo "C.FICHE.POST</br>";
            var_dump($_REQUEST);}
        
        $ficheActuelle->ajoutLigneForfait($_REQUEST['choixtypeforfait'], $_REQUEST['quantite']);
        $ficheActuelle->syncDatabase();
        
        $date = getDate();
        $month = $date['month'];
         
        include("view/v_header_navbar.php");
        include("view/v_edit_fiche.php");
        include("view/v_footer.php");
        
        if(DEBUGMOD){
            echo "CFICHE.SESS.UTIL";
            var_dump($_SESSION['utilisateur']);}
        }
        
        function ajoutLigneHorsForfait(){
            
            $ficheActuelle = $_SESSION['utilisateur']->getFicheDeFraisActive();
            if(DEBUGMOD){
                echo "C.FICHE.FICHE.ACT</br>";
                var_dump($ficheActuelle);}
            if(DEBUGMOD){
                echo "C.FICHE.POST</br>";
                var_dump($_REQUEST);}

            $ficheActuelle->ajoutLigneHorsForfait($ficheActuelle->getIdVisiteur(), $ficheActuelle->getMois(), $_REQUEST['libelle'], $_REQUEST['annee'].'/'.$_REQUEST['mois'].'/'.$_REQUEST['jour'], $_REQUEST['montant']);
            $ficheActuelle->syncDatabase();

            $date = getDate();
            $month = $date['month'];

            include("view/v_header_navbar.php");
            include("view/v_edit_fiche.php");
            include("view/v_footer.php");

            if(DEBUGMOD){
                echo "CFICHE.SESS.UTIL";
                var_dump($_SESSION['utilisateur']);}
            
        }
	
	function afficherFiche(){
	    //if(empty($_POST['month_select']))
	    //{
		$month = $_POST['month_select'];
		$date = getDate();
		$year = $date['year'];
		foreach($_SESSION['utilisateur']->getListeFicheDeFrais() as $f){
		    if($f->getMois() == $month){
			$fiche = $f;
		    }
		}
		include("view/v_header_navbar.php");
		include("view/v_fiche_liste.php");
		include("view/v_footer.php");
	    //}
	}
?>
