<?php

/** 
 * controleur de connexion. 
 
 * Permet la verification des identifiants
 * et selon le resultat la création d'une connexion sécurisé 
 * grace à un system de token,
 * ou le cas échant l'affichage d'une page d'erreur
 * 
 * 
 
 * @package default
 * @author CEC
 * @version    0.1A
 */

/**
 * Retourne les informations d'un visiteur
 
 * @param $param
 * @return le return
*/

//require_once("/include/modele/Utilisateur.php");

    /**
    * Action par defaut

    * @param $param
    * @return le return
    */

    function index() {
        
        if (estConnecte()){
                    include("view/v_header_navbar.php");
                    include("view/v_home.php");
                    include("view/v_footer.php");
        } else if (!empty($_REQUEST['login']) && !empty($_REQUEST['password'])) {
            $utilisateur = openSession($_REQUEST['login'], $_REQUEST['password']);
            if(DEBUGMOD){ echo "VAR DUMP OBJET UTILISATEUR FUNCTION INDEX -> c_connexion";
                var_dump($utilisateur);}
            
                if($utilisateur == FALSE){
                    include("vues/v_erreurLogin.php");
                    include("vues/v_connexion.php");
                } else {
                    $_SESSION['utilisateur'] = $utilisateur;
                    include("view/v_header_navbar.php");
                    include("view/v_home.php");
                    include("view/v_footer.php");
                    
                    //include("vues/v_sommaire.php");
                }
            
        } else {
            include("view/v_header_navbar.php");
            include("view/v_connexion.php");
            include("view/v_footer.php");
        }
    }
    

    

    /**
    * Initialise la session et met à jour la base de données

    * @param $param
    * @return le return
    */
    
    function openSession($login, $password) {
        $connectionGo = Utilisateur::checkLoginPassword($login, $password);
        
        if ($connectionGo == FALSE){
            if(DEBUGMOD){ echo "C.CONN.OPENSESS.CONNEC.GO=FALSE</br>";}
            return FALSE;
        } else {
            
            if(DEBUGMOD){ echo "C.CONN.OPENSESS.CONNEC.GO=TRUE</br>";}
            if(Visiteur::isVisiteur($connectionGo)){
                
                if(DEBUGMOD){ echo "C.CONN.OPENSESS.ISVIS=TRUE";}
                return new Visiteur($connectionGo);
                
            }else if(Comptable::isComptable($connectionGo)){
                
                return new Comptable ($connectionGo);
                
            }else{
                
                return new Utilisateur ($connectionGo);
                
            }
                
        }
    }

    
    /**
    * Récupère les informations concernant l'utilisateur

    * @param $param
    * @return le return
    */
   // getUserInfo(){}
    
    /**
    * Affiche le formulaire d'identification

    * @param $param
    * @return le return
    */
   //getFormView(){}
    
    /**
    * Affiche un message en cas de succès d'indentification 
    * ou déjà 'loggé'
    
    * @param $param
    * @return le return
    */
    //getMsg(){}
    
    /**
    * Supprime automatiquement les enregistrements vieux
    
    * @param $param
    * @return le return
    */
    //dbClean(){}
    
    /**
    * Permet de se deconnecter
    
    * @param $param
    * @return le return
    */
    function deconnexion(){
        if(DEBUGMOD){
        echo "SESSION AVANT DECO";
        var_dump($_SESSION['utilisateur']);}
        
        unset($_SESSION['utilisateur']);
        
        if(DEBUGMOD){
        echo "SESSION APRES DECO 1";
        var_dump($_SESSION['utilisateur']);}
        
        session_destroy();
        
        if(DEBUGMOD){
        echo "SESSION APRES DECO 2";
        var_dump($_SESSION['utilisateur']);}
        
                    include("view/v_header_navbar.php");
                    include("view/v_deconnexion.php");
                    include("view/v_footer.php");
        
    }
?>
